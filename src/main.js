var board = {
  sizeX: 640,
  sizeY: 480
}

var texts = {
  font: null,
  large: 100,
  medium: 50,
  small: 25,
  intro: 'THIS SITE\nIS UNDER\nCONSTRUCTION',
  ex: '!',
  gotit: 'GOT IT, THANKS',
  html: 'html',
  head: 'head',
  title: 'title',
  body: 'body',
  h1: 'h1',
  p: 'p',
  website: 'page',
  display: [
    '<!DOCTYPE html>\n<html>\n  <head>\n    <title></title>\n  </head>\n  <body>\n    <h1></h1>\n    <p></p>\n  </body>\n</html>',
    '<!DOCTYPE html>',
    '\n<html>\n\n\n\n\n\n\n\n</html>',
    '\n\n  <head>\n\n  </head>',
    '\n\n\n\n\n  <body>\n\n\n  </body>',
    '\n\n\n    <title></title>',
    '\n\n\n\n\n\n    <h1></h1>',
    '\n\n\n\n\n\n\n    <p></p>'
  ],
  congrat: 'NICE WORK!\nTHE SITE IS READY.',
  visit: 'VISIT IT'
}

var sounds = {
  bool: true,
  grab: null,
  drop: null,
  moveIn: null,
  moveOn: null,
  moveOut: null,
  slideIn: null,
  slideOn: null,
  slideOut: null,
  enter: null,
  background: null,
}

var colors = {
  blue: [3, 80, 158],
  black: [0, 0, 0],
  white: [255, 255, 255],
  yellow: [255, 255, 0],
}

var keys = {
  right: 39,
  left: 37,
  up: 38,
  down: 40,
  space: 32,
}

var states = {
  intro: true,
  construction: false,
  outro: false,
}

var player = {
  pos: -0.97,
  rot: -0.69,
}

var website = {
  pos: -2.57,
  rot: -0.87,
  size: board.sizeY * 0.4,
  html: {
    pos: -2.6,
    rot: 0.93,
    size: board.sizeY * 0.4,
    grabbed: false,
    placed: false,
  },
  head: {
    pos: -1.84,
    rot: 0.04,
    size: board.sizeY * 0.075,
    grabbed: false,
    placed: false,
  },
  body: {
    pos: -2.13,
    rot: 4.14,
    size: board.sizeY * 0.3,
    grabbed: false,
    placed: false,
  },
  title: {
    pos: -1.28,
    rot: 1.63,
    size: board.sizeY * 0.05,
    grabbed: false,
    placed: false,
  },
  h1: {
    pos: -1.87,
    rot: 2.64,
    size: board.sizeY * 0.15,
    grabbed: false,
    placed: false,
  },
  p: {
    pos: -2.47,
    rot: 3.04,
    size: board.sizeY * 0.075,
    grabbed: false,
    placed: false,
  },
}

var area = {
  size: 16,
}

function preload() {
  texts.font = loadFont('font/ibm.ttf')
  sounds.enter = loadSound('sound/enter.wav')
  sounds.grab = loadSound('sound/grab.wav')
  sounds.drop = loadSound('sound/drop.wav')
  sounds.moveIn = loadSound('sound/moving_in.wav')
  sounds.moveOn = loadSound('sound/moving_on.wav')
  sounds.moveOut = loadSound('sound/moving_out.wav')
  sounds.slideIn = loadSound('sound/slide_in.wav')
  sounds.slideOn = loadSound('sound/slide_on.wav')
  sounds.slideOut = loadSound('sound/slide_out.wav')
  sounds.background = loadSound('sound/background.wav')
}

function setup() {
  createCanvas(board.sizeX, board.sizeY)
}

function draw() {
  createCanvas(board.sizeX, board.sizeY)
  background(colors.blue)
  textFont(texts.font)
  textAlign(CENTER)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  // player actions
  if (keyIsDown(RIGHT_ARROW)) {
    player.rot += 0.005
    // html
    if (website.html.grabbed === true) {
      website.html.rot += 0.005
    }
    // head
    if (website.head.grabbed === true) {
      website.head.rot += 0.005
    }
    // body
    if (website.body.grabbed === true) {
      website.body.rot += 0.005
    }
    // title
    if (website.title.grabbed === true) {
      website.title.rot += 0.005
    }
    // h1
    if (website.h1.grabbed === true) {
      website.h1.rot += 0.005
    }
    // p
    if (website.p.grabbed === true) {
      website.p.rot += 0.005
    }
  }
  if (keyIsDown(LEFT_ARROW)) {
    player.rot -= 0.005
    // html
    if (website.html.grabbed === true) {
      website.html.rot -= 0.005
    }
    // head
    if (website.head.grabbed === true) {
      website.head.rot -= 0.005
    }
    // body
    if (website.body.grabbed === true) {
      website.body.rot -= 0.005
    }
    // title
    if (website.title.grabbed === true) {
      website.title.rot -= 0.005
    }
    // h1
    if (website.h1.grabbed === true) {
      website.h1.rot -= 0.005
    }
    // p
    if (website.p.grabbed === true) {
      website.p.rot -= 0.005
    }
  }
  if (keyIsDown(UP_ARROW)) {
    if (player.pos >= -3) {
      player.pos -= 0.005
    } else {
      player.pos = -3
    }
    // html
    if (website.html.grabbed === true) {
      if (website.html.pos >= -3) {
        website.html.pos -= 0.005
      } else {
        website.html.pos = -3
      }
    }
    // head
    if (website.head.grabbed === true) {
      if (website.head.pos >= -3) {
        website.head.pos -= 0.005
      } else {
        website.head.pos = -3
      }
    }
    // body
    if (website.body.grabbed === true) {
      if (website.body.pos >= -3) {
        website.body.pos -= 0.005
      } else {
        website.body.pos = -3
      }
    }
    // title
    if (website.title.grabbed === true) {
      if (website.title.pos >= -3) {
        website.title.pos -= 0.005
      } else {
        website.title.pos = -3
      }
    }
    // h1
    if (website.h1.grabbed === true) {
      if (website.h1.pos >= -3) {
        website.h1.pos -= 0.005
      } else {
        website.h1.pos = -3
      }
    }
    // p
    if (website.p.grabbed === true) {
      if (website.p.pos >= -3) {
        website.p.pos -= 0.005
      } else {
        website.p.pos = -3
      }
    }
  }
  if (keyIsDown(DOWN_ARROW)) {
    if (player.pos <= -0.3) {
      player.pos += 0.005
    } else {
      player.pos = -0.3
    }
    // html
    if (website.html.grabbed === true) {
      if (website.html.pos <= -0.3) {
        website.html.pos += 0.005
      } else {
        website.html.pos = -0.3
      }
    }
    // head
    if (website.head.grabbed === true) {
      if (website.head.pos <= -0.3) {
        website.head.pos += 0.005
      } else {
        website.head.pos = -0.3
      }
    }
    // body
    if (website.body.grabbed === true) {
      if (website.body.pos <= -0.3) {
        website.body.pos += 0.005
      } else {
        website.body.pos = -0.3
      }
    }
    // title
    if (website.title.grabbed === true) {
      if (website.title.pos <= -0.3) {
        website.title.pos += 0.005
      } else {
        website.title.pos = -0.3
      }
    }
    // h1
    if (website.h1.grabbed === true) {
      if (website.h1.pos <= -0.3) {
        website.h1.pos += 0.005
      } else {
        website.h1.pos = -0.3
      }
    }
    // p
    if (website.p.grabbed === true) {
      if (website.p.pos <= -0.3) {
        website.p.pos += 0.005
      } else {
        website.p.pos = -0.3
      }
    }
  }
  // intro
  if (states.intro === true) {
    push()
    translate(0, -board.sizeY * 0.1)
    // text
    textAlign(LEFT)
    fill(colors.white)
    noStroke()
    textSize(texts.medium)
    text(texts.intro, board.sizeX * 0.4, board.sizeY * 0.45)
    // sign
    fill(colors.yellow)
    stroke(colors.black)
    strokeWeight(16)
    triangle(board.sizeX * 0.1, board.sizeY * 0.7, board.sizeX * 0.35, board.sizeY * 0.7, board.sizeX * 0.225, board.sizeY * 0.4)
    // exclamation mark
    textAlign(CENTER)
    fill(colors.black)
    noStroke()
    textSize(texts.large)
    text(texts.ex, board.sizeX * 0.225, board.sizeY * 0.65)
    pop()
    // text
    textAlign(CENTER)
    noStroke()
    textSize(texts.small)
    if (mouseX > board.sizeX * 0.35 && mouseX < board.sizeX * 0.65 && mouseY > board.sizeY * 0.8 && mouseY < board.sizeY * 0.9) {
      fill(colors.yellow)
    } else {
      fill(colors.white)
    }
    text(texts.gotit, board.sizeX * 0.505, board.sizeY * 0.855)
  }
  // construction page
  if (states.construction === true) {
    // crane base
    noFill()
    stroke(colors.white)
    strokeWeight(2)
    rect(board.sizeX * 0.5, board.sizeY * 0.5, 64, 64)
    // goal website
    push()
    translate(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos)
    if (website.html.placed === false) {
      noFill()
      stroke(colors.yellow)
      ellipse(0, 0, website.size)
    }
    fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
    noStroke()
    textSize(texts.small)
    text(texts.website, 0, -website.size * 0.5 - 8)
    pop()
    // html
    push()
    noFill()
    stroke(colors.white)
    translate(board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos)
    ellipse(0, 0, website.html.size)
    if (website.html.placed === false) {
      fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
      noStroke()
      textSize(texts.small)
      text(texts.html, 0, -website.html.size * 0.5 - 8)
      if (website.html.grabbed === false) {
        noFill()
        stroke(colors.yellow)
        strokeWeight(2)
        ellipse(0, 0, area.size)
      }
    }
    pop()
    // head
    push()
    noFill()
    stroke(colors.white)
    translate(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos)
    ellipse(0, 0, website.head.size)
    if (website.head.placed === false) {
      fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
      noStroke()
      textSize(texts.small)
      text(texts.head, 0, -website.head.size * 0.5 - 8)
      if (website.head.grabbed === false) {
        noFill()
        stroke(colors.yellow)
        strokeWeight(2)
        ellipse(0, 0, area.size)
      }
    }
    pop()
    // body
    push()
    translate(board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos)
    noFill()
    stroke(colors.white)
    ellipse(0, 0, website.body.size)
    if (website.body.placed === false) {
      fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
      noStroke()
      textSize(texts.small)
      text(texts.body, 0, -website.body.size * 0.5 - 8)
      if (website.body.grabbed === false) {
        noFill()
        stroke(colors.yellow)
        strokeWeight(2)
        ellipse(0, 0, area.size)
      }
    }
    pop()
    // title
    push()
    translate(board.sizeX * 0.5 + sin(website.title.rot) * board.sizeY * 0.15 * website.title.pos, board.sizeY * 0.5 - cos(website.title.rot) * board.sizeY * 0.15 * website.title.pos)
    noFill()
    stroke(colors.white)
    ellipse(0, 0, website.title.size)
    if (website.title.placed === false) {
      fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
      noStroke()
      textSize(texts.small)
      text(texts.title, 0, -website.title.size * 0.5 - 8)
      if (website.title.grabbed === false) {
        noFill()
        stroke(colors.yellow)
        strokeWeight(2)
        ellipse(0, 0, area.size)
      }
    }
    pop()
    // h1
    push()
    translate(board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos)
    noFill()
    stroke(colors.white)
    ellipse(0, 0, website.h1.size)
    if (website.h1.placed === false) {
      fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
      noStroke()
      textSize(texts.small)
      text(texts.h1, 0, -website.h1.size * 0.5 - 8)
      if (website.h1.grabbed === false) {
        noFill()
        stroke(colors.yellow)
        strokeWeight(2)
        ellipse(0, 0, area.size)
      }
    }
    pop()
    // p
    push()
    translate(board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos)
    noFill()
    stroke(colors.white)
    ellipse(0, 0, website.p.size)
    if (website.p.placed === false) {
      fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
      noStroke()
      textSize(texts.small)
      text(texts.p, 0, -website.p.size * 0.5 - 8)
      if (website.p.grabbed === false) {
        noFill()
        stroke(colors.yellow)
        strokeWeight(2)
        ellipse(0, 0, area.size)
      }
    }
    pop()
    // crane rod
    push()
    translate(board.sizeX * 0.5 - sin(player.rot) * board.sizeY * 0.15, board.sizeY * 0.5 + cos(player.rot) * board.sizeY * 0.15)
    rotate(player.rot)
    fill(colors.blue)
    stroke(colors.white)
    strokeWeight(2)
    rect(0, 0, 10, board.sizeY * 0.6)
    pop()
    // crane cabin
    push()
    translate(board.sizeX * 0.5, board.sizeY * 0.5)
    rotate(player.rot)
    fill(colors.blue)
    stroke(colors.white)
    strokeWeight(2)
    rect(0, 0, 32, 32)
    rect(0, 0, 24, 24)
    pop()
    // crane counterweight
    push()
    translate(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15)
    rotate(player.rot)
    fill(colors.blue)
    stroke(colors.white)
    strokeWeight(2)
    for (var i = 0; i < 5; i++) {
      push()
      translate(0, -i * 8)
      if (i < 4) {
        rect(0, 0, 16, 8)
      } else {
        rect(0, 0, 8, 8)
      }
      pop()
    }
    pop()
    // player
    push()
    fill(colors.yellow)
    noStroke()
    translate(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos)
    ellipse(0, 0, 8, 8)
    pop()
    // crane cables
    push()
    translate(board.sizeX * 0.5, board.sizeY * 0.5)
    rotate(player.rot)
    fill(colors.blue)
    stroke(colors.white)
    strokeWeight(2)
    push()
    rotate(Math.PI * 0.075)
    rect(-12, -30, 1, 40)
    pop()
    push()
    rotate(-Math.PI * 0.075)
    rect(12, -30, 1, 40)
    pop()
    push()
    rotate(-Math.PI * 0.025)
    rect(-12, 76, 1, 128)
    pop()
    push()
    rotate(Math.PI * 0.025)
    rect(12, 76, 1, 128)
    pop()
    pop()
    // display html code
    fill(colors.white[0], colors.white[1], colors.white[2], 0.5)
    noStroke()
    textAlign(LEFT)
    textSize(texts.small * 0.5)
    text(texts.display[0], 32, 16 + texts.small)
    fill(colors.white)
    text(texts.display[1], 32, 16 + texts.small)
    if (website.html.placed === true) {
      text(texts.display[2], 32, 16 + texts.small)
    }
    if (website.head.placed === true) {
      text(texts.display[3], 32, 16 + texts.small)
    }
    if (website.body.placed === true) {
      text(texts.display[4], 32, 16 + texts.small)
    }
    if (website.title.placed === true) {
      text(texts.display[5], 32, 16 + texts.small)
    }
    if (website.h1.placed === true) {
      text(texts.display[6], 32, 16 + texts.small)
    }
    if (website.p.placed === true) {
      text(texts.display[7], 32, 16 + texts.small)
    }
  }
  // outro
  if (states.outro === true) {
    fill(colors.white)
    noStroke()
    textSize(texts.medium)
    textAlign(CENTER)
    text(texts.congrat, board.sizeX * 0.5, board.sizeY * 0.45)
    textSize(texts.small)
    if (mouseX > board.sizeX * 0.4 && mouseX < board.sizeX * 0.6 && mouseY > board.sizeY * 0.8 && mouseY < board.sizeY * 0.875) {
      fill(colors.yellow)
    } else {
      fill(colors.white)
    }
    text(texts.visit, board.sizeX * 0.5, board.sizeY * 0.85)
  }
  // border
  noFill()
  stroke(colors.black)
  strokeWeight(32)
  rect(board.sizeX * 0.5, board.sizeY * 0.5, board.sizeX, board.sizeY)
}

function keyReleased() {
  if (states.construction === true) {
    if (keyCode === keys.right) {
      sounds.moveOn.stop()
      playSound(sounds.moveOut)
    }
    if (keyCode === keys.left) {
      sounds.moveOn.stop()
      playSound(sounds.moveOut)
    }
    if (keyCode === keys.up) {
      sounds.slideOn.stop()
      playSound(sounds.slideOut)
    }
    if (keyCode === keys.down) {
      sounds.slideOn.stop()
      playSound(sounds.slideOut)
    }
  }
}

function keyPressed() {
  if (states.construction === true) {
    if (keyCode === keys.right) {
      playSound(sounds.moveIn)
      sounds.moveOn.loop()
    }
    if (keyCode === keys.left) {
      playSound(sounds.moveIn)
      sounds.moveOn.loop()
    }
    if (keyCode === keys.up) {
      playSound(sounds.slideIn)
      sounds.slideOn.loop()
    }
    if (keyCode === keys.down) {
      playSound(sounds.slideIn)
      sounds.slideOn.loop()
    }
    if (keyCode === keys.space) {
      // html
      if (dist(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos) <= 6 && website.html.grabbed === false && website.html.placed === false) {
        playSound(sounds.grab)
        setTimeout(function() {
          website.html.grabbed = true
        }, 10)
      }
      if (website.html.grabbed === true) {
        website.html.grabbed = false
        playSound(sounds.drop)
        if (dist(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos) <= 6) {
          website.html.placed = true
          website.html.rot = website.rot
          website.html.pos = website.pos
          playSound(sounds.enter)
        }
      }
      // head
      if (dist(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos) <= 6 && website.head.grabbed === false && website.head.placed === false) {
        playSound(sounds.grab)
        setTimeout(function() {
          website.head.grabbed = true
        }, 10)
      }
      if (website.head.grabbed === true) {
        playSound(sounds.drop)
        website.head.grabbed = false
        if (dist(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos) <= (website.size - website.head.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos) >= (website.head.size + website.body.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos) <= (website.html.size - website.head.size) * 0.5) {
          website.head.placed = true
          playSound(sounds.enter)
        }
      }
      // body
      if (dist(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos) <= 6 && website.body.grabbed === false) {
        playSound(sounds.grab)
        setTimeout(function() {
          website.body.grabbed = true
        }, 10)
      }
      if (website.body.grabbed === true) {
        playSound(sounds.drop)
        website.body.grabbed = false
        if (dist(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos) <= (website.size - website.body.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos) <= (website.html.size - website.body.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos) >= (website.head.size + website.body.size) * 0.5) {
          website.body.placed = true
          playSound(sounds.enter)
        }
      }
      // title
      if (dist(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeX * 0.5 + sin(website.title.rot) * board.sizeY * 0.15 * website.title.pos, board.sizeY * 0.5 - cos(website.title.rot) * board.sizeY * 0.15 * website.title.pos) <= 6 && website.title.grabbed === false) {
        playSound(sounds.grab)
        setTimeout(function() {
          website.title.grabbed = true
        }, 10)
      }
      if (website.title.grabbed === true) {
        playSound(sounds.drop)
        website.title.grabbed = false
        if (dist(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeX * 0.5 + sin(website.title.rot) * board.sizeY * 0.15 * website.title.pos, board.sizeY * 0.5 - cos(website.title.rot) * board.sizeY * 0.15 * website.title.pos) <= (website.size - website.title.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeX * 0.5 + sin(website.title.rot) * board.sizeY * 0.15 * website.title.pos, board.sizeY * 0.5 - cos(website.title.rot) * board.sizeY * 0.15 * website.title.pos) <= (website.html.size - website.title.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeX * 0.5 + sin(website.title.rot) * board.sizeY * 0.15 * website.title.pos, board.sizeY * 0.5 - cos(website.title.rot) * board.sizeY * 0.15 * website.title.pos) <= (website.head.size - website.title.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeX * 0.5 + sin(website.title.rot) * board.sizeY * 0.15 * website.title.pos, board.sizeY * 0.5 - cos(website.title.rot) * board.sizeY * 0.15 * website.title.pos) >= (website.body.size + website.title.size) * 0.5) {
          website.title.placed = true
          playSound(sounds.enter)
        }
      }
      // h1
      if (dist(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos) <= 6 && website.h1.grabbed === false) {
        playSound(sounds.grab)
        setTimeout(function() {
          website.h1.grabbed = true
        }, 10)
      }
      if (website.h1.grabbed === true) {
        playSound(sounds.drop)
        website.h1.grabbed = false
        if (dist(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos) <= (website.size - website.h1.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos) <= (website.html.size - website.h1.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos) <= (website.body.size - website.h1.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos) >= (website.h1.size + website.p.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos) >= (website.head.size + website.h1.size) * 0.5) {
          website.h1.placed = true
          playSound(sounds.enter)
        }
      }
      // p
      if (dist(board.sizeX * 0.5 + sin(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeY * 0.5 - cos(player.rot) * board.sizeY * 0.15 * player.pos, board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos) <= 6 && website.p.grabbed === false) {
        playSound(sounds.grab)
        setTimeout(function() {
          website.p.grabbed = true
        }, 10)
      }
      if (website.p.grabbed === true) {
        playSound(sounds.drop)
        website.p.grabbed = false
        if (dist(board.sizeX * 0.5 + sin(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeY * 0.5 - cos(website.rot) * board.sizeY * 0.15 * website.pos, board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos) <= (website.size - website.p.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeY * 0.5 - cos(website.html.rot) * board.sizeY * 0.15 * website.html.pos, board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos) <= (website.html.size - website.p.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeY * 0.5 - cos(website.body.rot) * board.sizeY * 0.15 * website.body.pos, board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos) <= (website.body.size - website.p.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeY * 0.5 - cos(website.h1.rot) * board.sizeY * 0.15 * website.h1.pos, board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos) >= (website.p.size + website.h1.size) * 0.5 && dist(board.sizeX * 0.5 + sin(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeY * 0.5 - cos(website.head.rot) * board.sizeY * 0.15 * website.head.pos, board.sizeX * 0.5 + sin(website.p.rot) * board.sizeY * 0.15 * website.p.pos, board.sizeY * 0.5 - cos(website.p.rot) * board.sizeY * 0.15 * website.p.pos) >= (website.head.size + website.p.size) * 0.5) {
          website.p.placed = true
          playSound(sounds.enter)
        }
      }
      // outro
      if (website.html.placed === true && website.head.placed === true && website.body.placed === true && website.title.placed === true && website.h1.placed === true && website.p.placed === true) {
        playSound(sounds.enter)
        setTimeout(function() {
          states.outro = true
          states.construction = false
        }, 10)
      }
    }
  }
}

function mousePressed() {
  if (states.intro === true) {
    if (mouseX > board.sizeX * 0.35 && mouseX < board.sizeX * 0.65 && mouseY > board.sizeY * 0.8 && mouseY < board.sizeY * 0.9) {
      states.intro = false
      states.construction = true
      playSound(sounds.enter)
      sounds.background.loop()
    }
  }
  if (states.outro === true) {
    if (mouseX > board.sizeX * 0.4 && mouseX < board.sizeX * 0.6 && mouseY > board.sizeY * 0.8 && mouseY < board.sizeY * 0.875) {
      window.location = 'http://aharonoff.gitlab.io/folio'
      playSound(sounds.enter)
    }
  }
}

function windowResized() {
  createCanvas(board.sizeX, board.sizeY)
}

function playSound(s){
  if(sounds.bool === true){
    s.play()
  }
}
